#!/bin/bash

conda build --no-test --py 27 .
conda build --no-test --py 35 .
conda build --no-test --py 36 .
