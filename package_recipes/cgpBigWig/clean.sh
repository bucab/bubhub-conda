PKG=cgpBigWig
CONDAROOT=/projectnb/bubhub/conda_root
CONDAPREFIX=`conda info -a | grep sys.prefix | sed 's/sys.prefix: //'`
conda remove ${PKG}  
grep -l ${PKG} ${CONDAROOT}/envs/.pkgs/cache/* | xargs -n 1 -I {} rm {}  

rm -irf ${CONDAROOT}/envs/.pkgs/${PKG}*
rm -irf ${CONDAROOT}/conda-bld/*/${PKG}*
rm -irf ${CONDAROOT}/conda-bld/src_cache/${PKG}*

rm -irf ${CONDAROOT}/envs/_build/lib/python3.5/site-packages/${PKG}*
rm -irf ${CONDAROOT}/envs/_test/lib/python3.5/site-packages/${PKG}*
rm -irf ${CONDAPREFIX}/lib/python3.5/site-packages/${PKG}*
