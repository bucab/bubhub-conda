# this only works on BU SCC!

# have to add boost paths manually as Makefile assumes they are available by default
BOOSTINC=/share/apps/6.0/boost/boost_1_51_0_gnu446_x86_64/include
BOOSTLIB=/share/apps/6.0/boost/boost_1_51_0_gnu446_x86_64/lib

# have to change cplex dir and cplex arch manually for some reason
sed -i 's:/opt/ibm/cplex12_5_1:/share/pkg/cplex/12.6.1/install:' Makefile
sed -i 's:x86-64_sles10_4.1:x86-64_linux:' Makefile
sed -i "s:^orman.o.*:& -I $BOOSTINC -L $BOOSTLIB:" Makefile

make -j

cp orman $PREFIX/bin
